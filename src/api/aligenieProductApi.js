import {
  req
} from './axiosFun';


export const AligenieProductList = (params) => {
  return req("get", "/aligenie/product/products", params)
};
// 保存产品
export const AligenieProductSave = (params) => {
  return req("post", "/aligenie/product/save", params)
};
// 取产品
export const GetAligenieProduct = (params) => {
  return req("get", "/aligenie/product/" + params, {})
};

